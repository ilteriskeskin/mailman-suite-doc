====================================
GNU Mailman 3.1 - List Member Manual
====================================

Mailman is free software for managing electronic mail discussion and
e-newsletter lists. Mailman is integrated with the web, making it easy for
users to manage their accounts and for list owners to administer their lists.
Mailman supports built-in archiving, automatic bounce processing, content
filtering, digest delivery, spam filters, and more.

This guide contains instructions for members of Mailman mailing lists so they
can learn to use the features available to them.  This focuses on the web
interface and includes sections on joining and leaving lists, editing options
and other subscriber-level tasks.

This guide is written for Mailman 3.1.  If you are using Mailman 2.1 (our
popular previous stable release), you should see `the Mailman 2.1 Members
Manual <https://wiki.list.org/DOC/Mailman%202.1%20Members%20Manual>`_.


Introduction to Mailman Suite
=============================

It may be easier to think of Mailman 3 as a single piece of software that does
mailing list management, but it's actually a set of interconnected pieces of
software under the hood.

The big pieces you care about as a user are as follows:

* **Mailman Core** - This is the "core" of Mailman that handles getting and
  sending email and stores all your email-related preferences.

* **Postorius** - This is the web interface to Mailman, which allows users to
  subscribe and unsubscribe from mailing lists and set preferences from the
  web.

* **Hyperkitty** - This is the archiver for Mailman, which allows users to view
  and interact with list archives from the web.

These were divided up so that you could replace pieces of Mailman as needed.
For example, if a site already had a user settings page, you might want to
run a modified version of Postorius so that users can set all their
preferences in one place, or you might want to replace the web interface
entirely.

This document is going to assume you're using these pieces together in their
default states.  If your Mailman installation is customized or has replaced
any of these pieces, you'll need to adjust the instructions accordingly.


Mailing List Terminology
------------------------

* A "**post**" typically denotes a message sent to a mailing list.

* People who are part of an electronic mailing list are usually called the
  list's "**members**" or "**subscribers**."

* "**List administrators**" are the people in charge of maintaining that one
  list. Lists may have one or more administrators.

* A list may also have people in charge of reading posts and deciding if they
  should be sent on to all subscribers. These people are called
  "**list moderators**."

* Often more than one electronic mailing list will be run using the same piece
  of software. The person who maintains the software which runs the lists is
  called the "**site administrator**." Often the same person who acts as site
  administrator also administrates individual lists.

Translating from our examples to real lists
-------------------------------------------

Often, it's easier to simply give an example than explain exactly how to
find the address for your specific list. As such, we'll frequently give
examples for a fictional list called LISTNAME@DOMAIN whose list information
page can be found at http://WEBSERVER/mailman3/lists/LISTNAME.DOMAIN.

Neither of these are real addresses, but they show the form of a typical list
address. The capital letters used for the list-specific parts of each address
should make it easier to see what should be changed for each list. Although
specific list configurations may be different, you will probably be able to
just replace the words given in capital letters with the appropriate values
for a real list:

LISTNAME
    The name of your list.

DOMAIN
    The name of the mail server which handles that list.

WEBSERVER
    The name of the web server which handles the list web interface. This may
    be the same as DOMAIN, and often refers to the same machine, but does not
    have to be identical.

As a real-life example, if you are interested in the mailman-users list that
runs on Mailman3.org, you'd make the following substitutions:
LISTNAME=mailman-users, DOMAIN=mailman3.org, WEBSERVER=lists.mailman3.org. As
such, for the mailman-users mailing list on mailman3.org, the list information
page can be found at the URL
http://lists.mailman3.org/mailman3/lists/mailman-users.mailman3.org/. (These,
unlike most of the examples given in this document, are real addresses.)

Most lists will have this information stored in the List-* headers. Many
mail programs will hide these by default, so you may have to choose to view
full headers before you can see these informational headers.


I need to talk to a human!
==========================

If you have any trouble with any of these commands, you can always reach the
person or people in charge of a list by using the list administrator email
address. The list administrators can help you figure out how to do something,
subscribe/unsubscribe you, or change your settings if you are unable to
change them yourself for some reason. Please remember that many mailing list
administrators are volunteers who are donating their spare time to run the
list, and they may be very busy people.

This list administrator email address is in the form LISTNAME-owner@DOMAIN,
where LISTNAME is the name of the list and DOMAIN is the
name of the server. So for an example list called <wolfhounds@example.com>
the administrators could be reached using <wolfhounds-owner@example.com>

This email address can also be found on the list information pages.


Making a Mailman account
========================

In order to manage your options and easily subscribe to or unsubscribe from
Mailman lists, you typically want to make an account.  There is a "sign up"
link on most list pages (usually displayed in the upper right of the page),
or you can go directly to the sign up interface at a URL that will be
something like  http://WEBSERVER/accounts/signup/

If you've been subscribed to a list without making an account (because you
did this yourself or because your lists were migrated from a Mailman 2.1
setup) you can make an account using the same email address and once you've
confirmed that you have access to that email, you'll be able to edit all the
associated options.

Note that in Mailman 3, you can actually have multiple email addresses
associated to the same user account, so you don't need to make many separate
accounts to handle your permissions.


Subscribing and Unsubscribing
=============================

How do I join a list?
---------------------
1. Go to the list information page for the list you want to join.  This will
   be something like http://WEBSERVER/mailman3/lists/LISTNAME.DOMAIN

2. Usually, it is a good idea to make an account first using the "sign up"
   option (on the upper right).  This account will allow you to change your
   settings later and make it easier for you to unsubscribe.

3. Once you are signed in, go back to the list information page and there
   will be a large section labelled "Subscribe to this list" where you can
   choose the email address you want to use and optionally choose a display
   name.  Fill out this form and click the "subscribe" button.

4. Alternatively, you can also join a list without signing in by using the
   subscribe boxes at the bottom of the list information page.  If you need to
   edit your settings later, you will need to create an account associated with
   the same email address.

How do I leave a list?
----------------------

1. Go to the list information page for the list you want to leave.  This will
   be something like http://WEBSERVER/mailman3/lists/LISTNAME.DOMAIN

2. Log in to confirm that you are the owner of the address that you wish to
   unsubscribe.  If you don't already have an account associated with that
   address, you may need to make one to prove that you are the correct owner of
   that email address.

3. Once you are logged in, there will be a section marked "Subscription /
   Unsubscription" that shows you the address which is subscribed to that list
   and a large "unsubscribe" button you can click to leave the list.


Changing your list settings
===========================

Mailman has a number of different settings for list subscribers as follows:

Delivery status
  Set this option to Enabled to receive messages posted to this mailing list.
  Set it to Disabled if you want to stay subscribed, but don't want mail
  delivered to you for a while (e.g. you're going on vacation). If you disable
  mail delivery, don't forget to re-enable it when you come back; it will not
  be automatically re-enabled.

Delivery mode
  If you select summary digests , you'll get posts bundled together (usually
  one per day but possibly more on busy lists), instead of singly when they're
  sent. Your mail reader may or may not support MIME digests. In general MIME
  digests are preferred, but if you have a problem reading them, select plain
  text digests.

Receive own postings
  Ordinarily, you will get a copy of every message you post to the list. If you
  don't want to receive this copy, set this option to No.  Note that some
  mail services (most prominently Gmail) will suppress this copy no matter
  what you do.  If you need to know when your email went through and your
  mail provider is blocking or removing the copy, you can also use the
  "Acknowledge posts" option to get a separate email acknowledging your post.

Acknowledge posts
  Receive acknowledgement mail when you send mail to the list?  The options
  are yes or no.  This is useful if your mail provider is making it difficult
  for you to know if your post has gone through.

Hide address
  When someone views the list membership, your email address is normally shown
  (in an obscured fashion to thwart spam harvesters). If you do not want your
  email address to show up on this membership roster at all, select Yes for
  this option.

Avoid Duplicates
  When you are listed explicitly in the To: or Cc: headers of a list message,
  you can opt to not receive another copy from the mailing list. Select Yes to
  avoid receiving copies from the mailing list; select No to receive copies.

Each of these settings can be set globally, per address, or per list.  Your
per-list settings over-ride the per-address settings which over-ride the
global settings.

How do I view my list settings?
-------------------------------
To change any settings, you can go to your settings page.

1. Log in to Mailman.

2. Click on the dropdown menu by your username (in the upper right) and
   select "Mailman settings"

3. Alternatively, the URL for this page will be something like
   http://WEBSERVER/mailman3/accounts/subscriptions/LISTNAME.DOMAIN


How do I disable/enable my mail delivery?
------------------------------------------

You may wish to temporarily stop getting messages from the list without
having to unsubscribe. If you disable mail delivery, you will no longer
receive messages, but will still be a subscriber and will retain your
other settings.

To disable or enable mail delivery from the web interface:

1. Log in and go to your list settings page

2. There is an option labelled "Delivery status" which you can enable or
   disable on your preferences tabs, either globally, per address, or per list
   subscription.


This can be handy in a many different cases. For example, you could be going
on vacation or need a break from the list because you're too busy to read any
extra mail. Many mailing lists also allow only subscribers to post to the
list, so if you commonly send mail from more than one address (e.g., one
address for at home and another for when you're travelling), you may want to
have more than one subscribed account, but have only one of them actually
receive mail. You can also use this as a way to read private archives even on
a list which may be too busy for you to have sent directly to your mailbox.
All you need to do is subscribe, disable mail delivery, and use your password
and email to log in to the archives.

How can I start or stop getting the list posts grouped into one big email?
--------------------------------------------------------------------------

Groups of posts are called "digests" in Mailman. Rather than get messages one
at a time, you can get messages grouped together. On a moderately busy list,
this typically means you get one email per day, although it may be more or
less frequent depending upon the list.

To change your digest settings:

1. Log in and go to your list settings page

2. There is an option labelled "Delivery Mode" which you can set on your
   preferences tabs, either globally, per address, or per list subscription.

There are a number of different options:

Regular
   You get an email every time the list sends one out.

Mime Digests
  MIME is short for Multipurpose Internet Mail Extensions. It is used to send
  things by email which are not necessarily simple plain text. (For example,
  MIME would be used if you were sending a picture of your dog to a friend.)
  A MIME digest has each message as an attachment inside the message, along
  with a summary table of contents.

Plain Text Digests
  A plain text digest is a simpler form of digest, which should be readable
  even in mail readers which don't support MIME. The messages are simply put
  one after the other into one large text message.

Most modern mail programs do support MIME, so you only need to choose plain
text digests if you are having trouble reading the MIME ones.


How do I stop or start getting copies of my own posts?
------------------------------------------------------
By default in Mailman, you get a copy of every post you send to the list.
Some people like this since it lets them know when the post has gone through
and means they have a copy of their own words with the rest of a discussion,
but others don't want to bother downloading copies of their own posts.

To receive or stop receiving your own posts:

1. Log in and go to your list settings page

2. There is an option labelled "Receive own postings" which you can set to
   yes or no on your preferences tabs, either globally, per address, or per list
   subscription.

Note: This option has no effect if you are receiving digests.


Despite the availability of this option, some mail hosts (such as Gmail) will
hide these posts from you.  You may wish to see "How can I get Mailman to
tell me when my post has been received by the list?" as an alternative
solution if your posts are being eaten by your mail host.


How can I get Mailman to tell me when my post has been received by the list?
----------------------------------------------------------------------------

On most lists, you will simply receive a copy of your mail when it has gone
through the list software, but if this is disabled, your list mail delivery
is disabled, you use a mail host such as Gmail which blocks copies of your
post from being received, or you simply want an extra acknowledgement from
the system, this option may be useful to you.

To receive or stop receiving your own posts:

1. Log in and go to your list settings page

2. There is an option labelled "Acknowledge Posts" which you can set to
   yes or no on your preferences tabs, either globally, per address, or per list
   subscription.

How can I hide my email address on the subscriber list?
-------------------------------------------------------

When someone views the list membership, your email address is normally shown
(in an obscured fashion to thwart spam harvesters), but you can hide the
address if you want:

1. Log in and go to your list settings page

2. There is an option labelled "Hide adresss" which you can set to
   yes or no on your preferences tabs, either globally, per address, or per list
   subscription.

Note that this does NOT hide your address in the list archives (if the list
has archives) or when it's sent out in emails, so a dedicated spammer could
probably get your address in other ways.


How can I avoid getting duplicate messages?
-------------------------------------------

Mailman can't completely stop you from getting duplicate messages, but it
can help. One common reason people get multiple copies of a mail is that the
sender has used a "group reply" function to send mail to both the list and
some number of individuals. If you want to avoid getting these messages,
Mailman can be set to check and see if you are in the To: or CC: lines of the
message. If your address appears there, then Mailman can be told not to
deliver another copy to you.

To avoid duplicates:

1. Log in and go to your list settings page

2. There is an option labelled "Avoid Duplicates" which you can set to yes
   or no on your preferences tabs, either globally, per address, or per list
   subscription.
